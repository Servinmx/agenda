<?php


namespace Models;


class Contacto extends Conexion
{
    public $id;
    public $nombre;
    public $apellido;
    public $telefono;
    public $correo;

    function insert()
    {
        $pre = mysqli_prepare($this->con, "INSERT INTO contactos (nombre, apellido, telefono, correo) VALUES (?, ?, ?, ?)");
        $pre->bind_param("ssss", $this->nombre, $this->apellido, $this->telefono, $this->correo);
        $pre->execute();
        if (!$pre) {
            echo "Error Inesperado";
        } else {
            echo "Su contacto fue registrado correctamente";
        }
    }
    function select(){
        $pre=mysqli_query($this->con, "SELECT * FROM contactos");
        $cont = 0;
        while($mostrar=mysqli_fetch_array($pre)){
            $cont = $cont + 1;
            $mostrar["id"]."\n";
            echo "Contanto No.".$cont."\n";
            echo "Nombre: ".$mostrar["nombre"]."\n";
            echo "Apellido: ".$mostrar["apellido"]."\n";
            echo "Telefono: ".$mostrar["telefono"]."\n";
            echo "Correo: ".$mostrar["correo"]."\n";
            echo "\n";
            echo "\n";
        }
    }
    public function consultar(){
        $contacto = new Contacto();
        $contacto->select();
    }
    function update()
    {
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "UPDATE contactos SET nombre=?,apellido=?,telefono=?,correo=? WHERE id=?");
        $pre -> bind_param("ssssi", $this->nombre, $this->apellido ,$this->telefono,$this->correo, $this->id);
        $pre ->execute();
        if (!$pre) {
            echo "Error Inesperado";
        } else {
            echo "Su contacto fue modificado correctamente";
        }
        return true;
    }
    static function remove($id)
    {
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "DELETE FROM contactos WHERE id=?");
        $pre->bind_param("i", $id);
        $pre->execute();
        if (!$pre) {
            echo "Error Inesperado";
        } else {
            echo "Su contacto fue eliminado correctamente";
        }
        return true;
    }
}