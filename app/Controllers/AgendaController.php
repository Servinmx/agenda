<?php

include "app/Models/Conexion.php";
include "app/Models/Contacto.php";

use Models\Contacto;
use Models\Conexion;
class agendaController
{
    public function agregar(){
        if(!isset($_POST["nombre"]) && !isset($_POST["apellido"]) && !isset($_POST["telefono"]) && !isset($_POST["correo"])){
            echo "Error, faltan datos para registrar contacto";
        }else{
            $contacto = new Contacto();
            $contacto->nombre = $_POST["nombre"];
            $contacto->apellido = $_POST["apellido"];
            $contacto->telefono = $_POST["telefono"];
            $contacto->correo = $_POST["correo"];
            $contacto->insert();
        }
    }
    public function consultar(){
        $contacto = new Contacto();
        $contacto->select();
    }
    public function eliminar()
    {
        $id = $_POST["id"];
        var_dump($id);
        $Contacto = Contacto::remove($id);
    }
    public function editar()
    {
        if (isset ($_POST)) {
            $id = $_POST["id"];
            $contacto = new Contacto();
            $contacto->nombre= $_POST["nombre"];
            $contacto->apellido = $_POST["apellido"];
            $contacto->telefono= $_POST["telefono"];
            $contacto->correo = $_POST ["correo"];
            $contacto -> id = $id;
            $contacto->update();
        }
    }
}